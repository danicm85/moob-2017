package elearning.com.moo_b.persistence;

import android.content.Context;
import android.util.Log;

import java.util.List;

import elearning.com.moo_b.model.Movie;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * Helper que encapsula el uso de Realm, consideradlo un DAO
 * <p>
 * Created by danielcruz on 12/12/2016.
 * <p>
 * Copyright © 2016 SIGMA AIE. All rights reserved.
 */

public class RealmHelper {


    private static final String TAG = "RealmHelper";
    private final Realm realm;

    public RealmHelper(Context context) {


        Realm.init(context); // ver la docu, mejor hacerlo en una instancia de Application https://realm.io/docs/java/latest/

        realm = Realm.getDefaultInstance();
    }


    /**
     * Recupera las peliculas. Cualquier cambio subsiguiente es notificado al listener
     *
     * @param changeListener objeto al cual notificar cambios en los resultados
     * @return la lista actual de peliculas, con el changelistener ya suscrito
     */
    public List<Movie> getFavorites(RealmChangeListener<RealmResults<Movie>> changeListener) {

        RealmResults<Movie> mResults = realm.where(Movie.class).findAll();
        mResults.addChangeListener(changeListener);
        return mResults;
    }


    /**
     * Comprueba si la pelicula indicada esta en base de datos o no
     *
     * @param id el imbd de la pelicula
     * @return true si esta en la base de datos, false en caso contrario
     */
    public boolean isFavorite(String id) {
        //realm no se trae ningun resultado hasta que no trabajas con los objetos en si.
        //no pasa nada por pedirle el count sobre una query
        return realm.where(Movie.class).equalTo("imdbID", id).count() > 0;


        // otra forma de hacerlo: preguntar por el primero (unico) con ese ID y ver si existe.
        // return getMovieById(id) != null;
    }

    /**
     * Recupera una pelicula según su id
     *
     * @param id el imbd
     * @return La pelicula o null
     */
    public Movie getMovieById(String id) {

        //Ejemplo de como sacar un elemento concreto de la BD, decimos que queremos buscar por imdbID, que es como se llama el campo dentro de Movie
        return realm
                .where(Movie.class)     //iniciamos una query de la que queremos sacar Movie
                .equalTo("imdbID", id)  //añadimos una condicion al 'where', y es que el imdbID = id
                .findFirst();           //que nos de el primero que encuentre. Sabemos que solo hay uno porque buscamos por primary key que por definicion es unica (como sql)
    }


    /**
     * Guardamos la pelicula indicada en la base de datos
     *
     * @param toSave la pelicula. Puede estar ya en favoritos en cuyo caso se actualizará
     */
    public void saveFavorite(Movie toSave) {

        // Intentaremos guardar en base de datos.
        // En este caso se incluye el código de proteccion de la excepcion

        try {

            realm.beginTransaction();
            //Si no hay primary key definida en el POJO, esto peta porque no puede distinguir los objetos entre sí
            realm.insertOrUpdate(toSave);
            realm.commitTransaction();

        } catch (RealmException re) {

            Log.e(TAG, "saveFavorite: ", re);
            realm.cancelTransaction();

        }


    }


    public void removeFromFavorite(Movie toRemove) {


        // si el objeto que recibimos no ha sido extraido directamente de la base de datos,
        // pasaremos a obtenerlo por id (por definicion, dos objetos con la misma primary key son el mismo)

        if (toRemove != null && !toRemove.isManaged()) {
            toRemove = getMovieById(toRemove.getImdbID());
        }


        if (toRemove != null) {
            realm.beginTransaction();
            toRemove.deleteFromRealm();
            realm.commitTransaction();
        }

    }


    public void close() {

        //esto lo haremos cuando la activity se destruya. siempre cerrad un realm cuando ya no vayais a usarlo mas
        realm.close();
    }


}
