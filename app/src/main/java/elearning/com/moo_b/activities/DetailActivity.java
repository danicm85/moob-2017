package elearning.com.moo_b.activities;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import elearning.com.moo_b.R;
import elearning.com.moo_b.model.Movie;
import elearning.com.moo_b.persistence.RealmHelper;
import elearning.com.moo_b.rest.RestHelper;

public class DetailActivity extends AppCompatActivity implements RestHelper.RestDelegate, View.OnClickListener {

    private static final String TAG = "DetailActivity";


    //blabla, monton de vistas.
    private ImageView poster;
    private CollapsingToolbarLayout toolbarLayout;
    private RestHelper restHelper;
    private RealmHelper realmHelper;
    private TextView genre;
    private TextView director;
    private TextView actors;
    private TextView plot;
    private TextView titleView;
    private ImageView bigPoster;
    private View bigPosterContainer;
    private FloatingActionButton fab;
    private Drawable favorite;
    private Drawable noFavorite;

    private Animation pulse;


    //aqui tenemos los datos conocidos al arrancar la activity
    private String imageUrl;
    private String title;
    private String id;

    //aqui, tendremos la pelicula si ya era favorita o cuando nos llegue del server
    private Movie mMovie;

    //flag para almacenar el estado temporal fav/nofav
    boolean isFav;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        restHelper = new RestHelper(this);
        realmHelper = new RealmHelper(this);
        pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);


    }


    private void load() {


    }


    /**
     * Este metodo extrae una paleta de colores del bitmap del poster. Lo usamos para dar color a la toolbar,
     * que de este modo dependera de los colores del poster. ver la docu oficial para mas info
     */
    private void setPalette() {


        //se hace con un callback porque puede tardar bastante en generarla.
        Palette.from(((BitmapDrawable) poster.getDrawable()).getBitmap()).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                toolbarLayout.setBackgroundColor(palette.getVibrantColor(toolbarLayout.getSolidColor()));
                toolbarLayout.setContentScrimColor(palette.getMutedColor(toolbarLayout.getSolidColor()));
            }
        });


    }


    @Override
    public void onMovieSearchResult(List<Movie> movies) {
        // nada
    }

    @Override
    public void onMovieSearchFailure() {
        // nada
    }

    @Override
    public void onMovieDetailResult(Movie movie) {


    }

    private void setupFab() {


        fab.setVisibility(View.VISIBLE);
        favorite = ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_favorite_white_24dp).mutate();
        noFavorite = ContextCompat.getDrawable(DetailActivity.this, R.drawable.ic_favorite_border_white_24dp);

        syncFavoriteDrawable();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //cuando el usuario haga click, alternaremos el estado
                isFav = !isFav;
                //cambiaremos el icono
                syncFavoriteDrawable();

                //y mostraremos una animación que deja claro que hemos tenido en cuenta la accion.
                //el boton aplicara las transformaciones indicadas a sus valores
                fab.startAnimation(pulse);
            }
        });


    }

    private void syncFavoriteDrawable() {

        //haremos que el fab refleje el estado real
        fab.setImageDrawable(isFav ? favorite : noFavorite);
    }


    @Override
    public void onBackPressed() {

        //Este metodo esta disponible para override en todas las activities.
        //indica que el usuario ha pulsado back.
        //podemos evitar el cierre NO LLAMANDO a super.onBackPressed().


        //en este caso, si estamos mostrando la version ampliada del poster, cerramos el poster,
        //y si no dejamos salir al usuario


        //nunca hay que impedir que el usuario abandone una activity sin un buen motivo, el dispositivo
        // es suyo y no debemos decidir cuando puede o no salir de nuestra app.

        //en este caso lo interceptamos porque, de ser el poster visible, lo mas probable es que el usuario
        //quiera que se quite de enmedio. En el resto de casos cerramos la activity (dejando que super.onBackPressed() haga su trabajo)

        if (bigPosterContainer.getVisibility() == View.VISIBLE) {
            bigPosterContainer.setVisibility(View.INVISIBLE);
        } else {

            super.onBackPressed();
        }

    }

    @Override
    public void onMovieDetailFailure() {
        // nada
    }

    @Override
    public void onClick(View v) {

        //si el usuario toca la foto grande o la barra de tareas, mostramos u ocultamos la imagen grande
        //ver el metodo load(), donde seteamos el mismo onClickListener para ambas vistas.
        if (bigPosterContainer.getVisibility() != View.VISIBLE) {

            bigPosterContainer.setVisibility(View.VISIBLE);
        } else {
            bigPosterContainer.setVisibility(View.INVISIBLE);
        }
    }
}
