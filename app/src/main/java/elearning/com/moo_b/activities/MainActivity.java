package elearning.com.moo_b.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import elearning.com.moo_b.R;
import elearning.com.moo_b.adapters.MovieAdapter;
import elearning.com.moo_b.favs.FavsActivity;
import elearning.com.moo_b.providers.SuggestionProvider;
import elearning.com.moo_b.rest.RestHelper;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";

    private RestHelper restHelper;
    private TextView banner;
    private RecyclerView list;
    private MovieAdapter adapter;
    private SearchView searchView;
    private MenuItem searchItemMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    private void searchByTitle(String title) {

    }

    @Override
    protected void onNewIntent(Intent intent) {

        // Este método se llama cada vez que nuestra activity recibe un nuevo intent.

        // Normalmente, solo recibimos uno, el que arranca la activity. En este caso
        // recibimos las búsquedas como un intent, por lo que necesitamos saber cada vez que llega uno nuevo.

        // ver la docu: https://developer.android.com/guide/topics/search/search-dialog.html


        // no es necesario utilizar estas modificaciones respecto al proyecto, pero es interesante ver una manera
        // de integrar la búsqueda de una forma más 'android'.
        // Podeis mantener el campo de busqueda y el searchIcon con su onclick, y la app funciona igual.


        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            //Recuperamos el texto solicitado por el usuario (Titulo de la pelicula a buscar)
            String query = intent.getStringExtra(SearchManager.QUERY);

            // Creamos una instancia de la clase que gestiona las sugerencias de busqueda (para mostrarlas al usuario)
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);

            //Guardamos la busqueda actual en las sugerencias, android se encarga de persistirlas y mostrarlas
            suggestions.saveRecentQuery(query, null);


            // aqui la logica de verdad
            searchByTitle(query);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // op estándar
        getMenuInflater().inflate(R.menu.menu_main, menu);


        // op copy paste de google.
        searchItemMenu = menu.findItem(R.id.search);
        //Lo que viene a continuacion esta copypasteado de la docu de google sobre la SearchView.
        //Usaremos lo que nos dicen a rajatabla, pero es un overkill ( vereis que le pasamos las busquedas por intent a nuestra propia activity).

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.my_favs) {
            startActivity(new Intent(this, FavsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setDataView(boolean hasData) {

        // Si determinamos que hay datos, nos aseguramos que la lista sea visible
        // y el banner de aviso desaparezca. Lo contrario si no hay datos.

        if (hasData) {
            banner.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        } else {
            banner.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
        }
    }


}
