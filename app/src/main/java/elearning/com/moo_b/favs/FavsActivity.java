package elearning.com.moo_b.favs;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.List;

import elearning.com.moo_b.R;
import elearning.com.moo_b.model.Movie;
import elearning.com.moo_b.persistence.RealmHelper;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class FavsActivity extends AppCompatActivity implements MoviePagerAdapter.DataDelegate, RealmChangeListener<RealmResults<Movie>> {

    private static final String TAG = "FavsActivity";

    private RealmHelper realm;
    private ViewPager viewPager;
    private MoviePagerAdapter mAdapter;
    private List<Movie> dataSet;
    private View noDataBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar supportBar = getSupportActionBar();
        if (supportBar != null) {
            supportBar.setDisplayHomeAsUpEnabled(true);
        }

        // preparamos la base de datos y obtenemos la lista de favoritos actuales
        realm = new RealmHelper(this);
        dataSet = realm.getFavorites(this);

        noDataBanner = findViewById(R.id.no_favs_banner);

        //encontramos el paginador
        viewPager = (ViewPager) findViewById(R.id.favorites_pager);
        viewPager.setOffscreenPageLimit(2);

        //montamos un adapter que irá dando los fragments segun pasamos paginas
        mAdapter = new MoviePagerAdapter(getSupportFragmentManager());
        mAdapter.setDelegate(this);

        //añadimos algo de efecto al pasar de una página a otra
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());

        //vinculamos los datos con la vista
        viewPager.setAdapter(mAdapter);

        syncBanner();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // si no esta vacio tenemos la opcion de eliminar
        if (!dataSet.isEmpty()) {
            getMenuInflater().inflate(R.menu.menu_favs, menu);
            return true;
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.delete_fav) {

            deleteCurrentPage();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteCurrentPage() {
        //recuperamos el num de pagina que estamos mostrando, y la vista correspondiente
        int position = viewPager.getCurrentItem();
        View currentView = viewPager.getChildAt(position);

        if (currentView != null) { //por si las moscas

            //averiguamos que peli estamos mostrando, y la eliminamos
            final Movie toDelete = dataSet.get(position);
            delayDelete(currentView, toDelete);

        }
    }

    private void delayDelete(View currentView, final Movie toDelete) {

        //cargamos una animación
        Animation coolAnimation = AnimationUtils.loadAnimation(this, R.anim.shrink);

        //establecemos un listener para actuar cuando la animación finalice
        coolAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //no-op
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //cuando acabe, eliminamos la peli.
                //si lo hacemos nada más seleccionar la opcion, los datos cambian
                //y la pantalla se repinta cancelando la animacion
                realm.removeFromFavorite(toDelete);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //no-op
            }
        });

        // ahora la iniciamos, cuando acabe se llamará al código anterior
        currentView.startAnimation(coolAnimation);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // liberamos la bd
        realm.close();
    }


    //ver clase adapter

    @Override
    public int getTotal() {
        return dataSet.size();
    }

    @Override
    public Movie getItemAtPosition(int position) {
        return dataSet.get(position);
    }


    //ver realmhelper.getFavorites(this)

    @Override
    public void onChange(RealmResults<Movie> element) {

        //cuando los favoritos cambian, notificamos a los interesados:

        // 1 ) el adapter que muestra los fragments en el viewpager
        mAdapter.notifyDataSetChanged();

        // 2 ) el menu de opciones, que puede que ya no deba mostrarse
        invalidateOptionsMenu();

        // 3 ) el texto que informa que no hay favoritos.
        syncBanner();

    }

    private void syncBanner() {
        noDataBanner.setVisibility(dataSet.isEmpty() ? View.VISIBLE : View.GONE);
    }
}
