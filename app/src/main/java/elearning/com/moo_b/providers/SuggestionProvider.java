package elearning.com.moo_b.providers;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by danielcruz on 09/12/2016.
 * <p>
 * Copyright © 2016 SIGMA AIE. All rights reserved.
 */


// esta clase es una fumada para que podamos sugerir busquedas previas.
// se escribe tal cual, google de hecho te da este código copy paste al que solo hay que modificar el AUTHORITY con el nombre de nuestra clase.


public class SuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "elearning.com.moo_b.providers.SuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}