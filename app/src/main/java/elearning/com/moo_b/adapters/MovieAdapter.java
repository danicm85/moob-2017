package elearning.com.moo_b.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import elearning.com.moo_b.R;
import elearning.com.moo_b.model.Movie;

/**
 * Created by danielcruz
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {


    private List<Movie> data;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView title;

        ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.row_movie_title);

        }
    }
}
